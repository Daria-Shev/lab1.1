﻿using System;

namespace Lab1._1
{
    public class Program
    {
        //Обчислити висоту циліндра, якщо задано його об’єм та площа основи.
        public static void Main(string[] args)
        {
            Console.Write("Введiть об'єм цилiндра: ");
            float volume = conversion_to_float();
            Console.Write("Введiть площу основи цилiндра: ");
            float base_area = conversion_to_float();
            check_area_more_0(base_area);
            float height = volume / base_area;
            infinity_check(height);
            Console.WriteLine($"Висота цилiндра: {height}");
        }

        static void check_area_more_0(float value)
        {
            if (value <= 0)
            {
                Console.WriteLine("Некоректнi данi. Площа основи цилiндра повинна бути додатньою.");
                Environment.Exit(0);
            }
        }
        static void infinity_check(float value)
        {
            if (float.IsInfinity(value))
            {
                Console.WriteLine("Введенi значення занадто великi або занадто малi.");
                Environment.Exit(0);
            }
        }
        static float conversion_to_float()
        {
            float value=0; 
            try
            {
                 value = float.Parse(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine("Некоректнi данi. Введiть числовi значення для об'єму та площi основи цилiндра.");
                Environment.Exit(0);
            }
            infinity_check(value);
            return value;
        }
    }
}
