﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lab1._1;
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Lab1._1.Tests
{
    [TestClass()]
    public class ProgramTests
    {
        private string console_interception(string value)
        {
            using (StringWriter sw = new StringWriter())
            {
                Console.SetOut(sw);
                Console.SetIn(new StringReader(value));
                string[] args = null;
                Program.Main(args);
                string consoleOutput = sw.ToString();
                return consoleOutput;
            }            
        }
        private string string_console_result(string consoleOutput)
        {
            string stringToRemoveVolume = "Введiть об'єм цилiндра: ";
            string stringToRemoveArea = "Введiть площу основи цилiндра: ";
            string ResultString = consoleOutput.Replace(stringToRemoveVolume, "").Replace(stringToRemoveArea, "").Replace("  ", " ");
            return ResultString;
        }

        [TestMethod()]
        public void division_6by3_2returned()
        {
            string value = "6\n3\n";
            string expectedOutput = $"Висота цилiндра: { 2}\r\n";
            string actualOutput = string_console_result(console_interception(value));
            Assert.AreEqual(expectedOutput, actualOutput);            
        }

        [TestMethod()]
        public void division_6by0_error_returned()
        {
            string value = "6\n0\n";
            string expectedOutput = "Некоректнi данi. Площа основи цилiндра повинна бути додатньою.\r\n";
            string actualOutput = string_console_result(console_interception(value));
            Assert.AreEqual(expectedOutput, actualOutput);            
        }

        [TestMethod()]
        public void division_Aby3_FormatException_returned()
        {
            string value = "A\n3\n";
            string expectedOutput = "Некоректнi данi. Введiть числовi значення для об'єму та площi основи цилiндра.\r\n";
            string actualOutput = string_console_result(console_interception(value));
            Assert.AreEqual(expectedOutput, actualOutput);            
        }

        [TestMethod()]
        public void division_MaxValue_by1_OverflowException_returned()
        {
            string value = $"{float.MaxValue + float.MaxValue}\n1\n";
            string expectedOutput = "Введенi значення занадто великi або занадто малi.\r\n";
            string actualOutput = string_console_result(console_interception(value));
            Assert.AreEqual(expectedOutput, actualOutput);            
        }
    }
}